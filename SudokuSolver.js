class Tile {
	constructor(x, y) {
		this.x = x
		this.y = y
		this.index = this.x*9 + this.y+1

		let v = document.getElementById('A' + this.index).value

		this.value = Number(v)
		this.solved = this.value > 0

		this.possibleValues = [
			null,
			true, true, true, true, true, true, true, true, true,
		]
	}

	removeFromPossibilities = (value) => {
		this.possibleValues[value] = false
	}

	set = (value) => {
		if (this.solved) {
			throw new Error('Already solved')
		}
		this.value = value
		this.solved = true

		document.getElementById('A' + this.index).value = value
		document.getElementById('Ai' + this.index).value = ''
	}

	printPossibilities = () => {
		let s = ''
		document.getElementById('Ai' + this.index).value = s
		if (this.solved) {
			return false
		}

		for (let i = 1; i <= 9; i++) {
			if (this.possibleValues[i]) {
				s += i
			}
		}

		document.getElementById('Ai' + this.index).value = s
		return true
	}

	isNaked = () => {
		if (this.solved) {
			return false
		}
		let isNaked = 1
		for (let i = 1; i <= 9; i++) {
			if (this.possibleValues[i]) {
				isNaked--
			}
		}
		return isNaked === 0
	}

	nakedValue = () => {
		if (!this.isNaked()) {
			throw new Error()
		}
		for (let i = 1; i <= 9; i++) {
			if (this.possibleValues[i]) {
				return i
			}
		}
		throw new Error()
	}

	// TODO: Turn this into property,
	// no need to calculate it every time.
	count = () => {
		let res = 0
		if (this.solved) {
			return res;
		}
		for (let i = 1; i <= 9; i++) {
			if (this.possibleValues[i]) {
				res++
			}
		}
		return res
	}
}

class TileMap {
	constructor(isInDevelopment, isDiagonal, isCenter) {
		this.isInDevelopment = isInDevelopment
		this.isDiagonal = isDiagonal
		this.isCenter = isCenter
		this.toBeSolved = 81
		this.map = []
		for (let i = 0; i < 9; i++) {
			this.map[i] = []
			for (let j = 0; j < 9; j++) {
				let t = new Tile(i, j)
				if (t.solved) {
					this.toBeSolved--
				}
				this.map[i][j] = t
			}
		}
	}

	setup = () => {
		for (let i = 0; i < 9; i++) {
			for (let j = 0; j < 9; j++) {
				let t = this.map[i][j]
				if (!t.solved) {
					continue
				}
				this.removeFromColumn(j, t.value)
				this.removeFromRow(i, t.value)
				this.removeFromBlock(i, j, t.value)
				if (this.isDiagonal) {
					this.removeFromDiagonal(i, j, t.value)
				}
				if (this.isCenter) {
					this.removeFromCenter(i, j, t.value)
				}
			}
		}
		this.printPossibilities()
	}

	// printPossibilities is a wrapper for a TileMap and
	// Tiles printPossibilities.
	// It is used in both setup, and remove.
	printPossibilities = () => {
		for (let i = 0; i < 9; i++) {
			for (let j = 0; j < 9; j++) {
				this.map[i][j].printPossibilities()
			}
		}
	}

	removeFromColumn = (col, value) => {
		for (let i = 0; i < 9; i++) {
			this.map[i][col].removeFromPossibilities(value)
		}
	}

	removeFromRow = (row, value) => {
		for (let i = 0; i < 9; i++) {
			this.map[row][i].removeFromPossibilities(value)
		}
	}

	removeFromBlock = (row, col, value) => {
		let x = Math.floor(row / 3) * 3
		row %= 3
		let y = Math.floor(col / 3) * 3
		col %= 3

		for (let i = 0; i < 3; i++) {
			for (let j = 0; j < 3; j++) {
				this.map[i + x][j + y].removeFromPossibilities(value)
			}
		}
	}

	removeFromDiagonal = (row, col, value) => {
		if (row === col) {
			for (let i = 0; i < 9; i++) {
				this.map[i][i].removeFromPossibilities(value)
			}
		}
		if (row + col === 8) {
			// From left bottom to upper right.
			for (let i = 0; i < 9; i++) {
				this.map[8 - i][i].removeFromPossibilities(value)
			}
		}
	}

	removeFromCenter = (row, col, value) => {
		if (row % 3 !== 1 || col % 3 !== 1) {
			return
		}
		for (let i = 1; i < 9; i += 3) {
			for (let j = 1; j < 9; j += 3) {
				this.map[i][j].removeFromPossibilities(value)
			}
		}
	}

	isFinished = () => {
		return this.toBeSolved <= 0
	}

	solve = () => {
		// Just a prereq for other steps.
		// TODO: Move this after hidden singles.
		// Turn it to something like claiming,
		// if something is removed, we are happy.
		for (let i = 0; i < 9; i++) {
			for (let j = 0; j < 9; j++) {
				this.pointing(i, j)
			}
		}

		// Solve naked singles first, then go
		// for the hidden ones.
		//
		// This looks more like a human behaviour,
		// go for the easy ones, then
		// try the more tricky ones.
		for (let i = 0; i < 9; i++) {
			for (let j = 0; j < 9; j++) {
				if (this.map[i][j].solved) {
					continue
				}

				if (this.nakedSingle(i, j)) {
					this.toBeSolved--
					return
				}
			}
		}

		for (let i = 0; i < 9; i++) {
			for (let j = 0; j < 9; j++) {
				if (this.map[i][j].solved) {
					continue
				}

				if (this.hiddenSingle(i, j)) {
					this.toBeSolved--
					return
				}
			}
		}

		// Extra more expensive checks
		// which I would like to turn later
		// on into something which should affect
		// the time overall.
		if (!this.claim()) {
			throw new Error('Too hard to be solved')
		}
		// Swapping this condition was really helpful,
		// now it even works, without ugly return this.solve()
		// which I had before, leading to stack overflow.
		console.log('Successful claiming.')
		this.solve()
	}

	claim = () => {
		console.log('Claim!')
		for (let i = 0; i < 9; i++) {
			let rows = new Array;
			let columns = new Array;
			for (let j = 0; j < 9; j++) {
				let t = this.map[i][j]
				if (!t.solved) {
					rows.push(t)
				}
				t = this.map[j][i]
				if (!t.solved) {
					columns.push(t)
				}
			}
			if (this.claiming(rows)) {
				// Three lines just for me, easier testing.
				console.log(i)
				console.log('Successful row claiming.')
				this.printPossibilities()

				return true
			}

			if (this.claiming(columns)) {
				// Three lines just for me, easier testing.
				console.log(i)
				console.log('Successful column claiming.')
				this.printPossibilities()

				return true
			}
		}

		// Blocks.
		for (let b1 = 0; b1 < 3; b1++) {
			for (let b2 = 0; b2 < 3; b2++) {
				let block = new Array;
				for (let i = 0; i < 3; i++) {
					for (let j = 0; j < 3; j++) {
						let t = this.map[3*b1 + i][3*b2 + j]
						if (!t.solved) {
							block.push(t)
						}
					}
				}
				if (this.claiming(block)) {
					// Three lines just for me, easier testing.
					console.log(3*b1 + ' - ' + 3*b2)
					console.log('Successful block claiming.')
					this.printPossibilities()

					return true
				}
			}
		}

		// Diagonals.
		if (this.isDiagonal) {
			let d = new Array;
			for (let i = 0; i < 9; i++) {
				let t = this.map[i][i];
				if (!t.solved) {
					d.push(t);
				}
			}
			if (this.claiming(d)) {
				console.log('Successful left down diagonal claiming.')
				this.printPossibilities()

				return true
			}

			d = new Array;
			for (let i = 0; i < 9; i++) {
				let t = this.map[i][8 - i];
				if (!t.solved) {
					d.push(t);
				}
			}
			if (this.claiming(d)) {
				console.log('Successful right down diagonal claiming.')
				this.printPossibilities()

				return true
			}
		}

		return false
	}

	remove = (row, col, value) => {
		this.removeFromRow(row, value)
		this.removeFromColumn(col, value)
		this.removeFromBlock(row, col, value)
		if (this.isDiagonal) {
			this.removeFromDiagonal(row, col, value)
		}
		if (this.isCenter) {
			this.removeFromCenter(row, col, value)
		}
		this.printPossibilities()
	}

	nakedSingle = (row, col) => {
		let t = this.map[row][col]
		if (!t.isNaked()) {
			return false
		}

		this.set(t, t.nakedValue(), row, col)
		return true
	}

	hiddenSingle = (row, col) => {
		let res = this.hiddenSingleRow(row)
		if (res) {
			return true
		}

		res = this.hiddenSingleColumn(col)
		if (res) {
			return true
		}

		res = this.hiddenSingleBlock(row, col)
		if (res) {
			return true
		}

		if (this.isDiagonal) {
			res = this.hiddenSingleDiagonal()
			if (res) {
				return true
			}
		}

		return false
	}

	hiddenSingleRow = (row) => {
		let indexes = []
		for (let i = 0; i < 9; i++) {
			indexes.push(this.map[row][i])
		}

		return this.hidden(indexes)
	}

	hiddenSingleColumn = (col) => {
		let indexes = []
		for (let i = 0; i < 9; i++) {
			indexes.push(this.map[i][col])
		}

		return this.hidden(indexes)
	}

	hiddenSingleBlock = (row, col) => {
		let x = Math.floor(row / 3) * 3
		row %= 3
		let y = Math.floor(col / 3) * 3

		let indexes = []
		for (let i = 0; i < 3; i++) {
			for (let j = 0; j < 3; j++) {
				indexes.push(this.map[x + i][y + j])
			}
		}
		return this.hidden(indexes)
	}

	hiddenSingleDiagonal = () => {
		let lr = []
		let rl = []
		for (let i = 0; i < 9; i++) {
			lr.push(this.map[i][i])
			rl.push(this.map[i][8 - i])
		}
		return this.hidden(lr) || this.hidden (rl)
	}

	// hidden takes array of tiles and checks if the values in the given
	// set are unique. If yes, it sets it up.
	//
	// This is the generic way how to find hidden singles, just check if
	// one of the possible values in the given tiles are unique.
	//
	// It works like a hungry algorithm, so if 1, 2, 3 are unique, only the
	// first one is set. Leaving it as it is for now, respecting the timer
	// in the function 'repeater'.
	hidden = (tiles) => {
		let possibleValues = new Array(10).fill(0)
		for (let t of tiles) {
			if (t.solved) {
				continue
			}
			let vals = t.possibleValues
			for (let j = 1; j <= 9; j++) {
				if (vals[j]) {
					possibleValues[j]++
				}
			}
		}

		let res = 0
		for (let i = 1; i <= 9; i++) {
			if (possibleValues[i] === 1) {
				res = i
				break
			}
		}
		if (res === 0) {
			return false
		}

		for (let t of tiles) {
			if (t.solved) {
				continue
			}
			let vals = t.possibleValues
			if (vals[res]) {
				this.set(t, res, t.x, t.y)
				return true
			}
		}
		return false
	}

	// Claiming tries to find some tile subset with similar
	// values which leads to removal of these values from other
	// tiles.
	//
	// Nice goal would be to get rid of some of the pointing stuff
	// which is not really a pointing and so on.
	claiming = (tiles) => {
		for (let rt of tiles) {
			// ref as reference tile.
			let ref = rt

			if (ref.count() >= tiles.length) {
				continue
			}

			let uniques = new Array;
			uniques.push(rt)

			for (let t of tiles) {
				if (ref.x === t.x && ref.y === t.y) {
					continue
				}

				if (t.count() > ref.count()) {
					continue
				}
				// What has 't' has to be in 'ref', too.
				let err = false
				for (let k = 1; k <= 9; k++) {
					if (t.possibleValues[k] && !ref.possibleValues[k]) {
						err = true
						break
					}
				}
				if (err) {
					continue
				}
				uniques.push(t)
			}
			if (uniques.length !== ref.count()) {
				continue
			}

			// Cleanup.
			// This does not always have to succeed, it is really common
			// that found N tiles already are at their minimum.
			let removed = false
			for (let t of tiles) {
				// c is the sign if we can continue in removing values.
				let c = true

				// Check if the index is not from new enum.
				// TODO: Uniques can be smarter.
				for (let k of uniques) {
					if (k.x === t.x && k.y === t.y) {
						c = false
						break
					}
				}
				if (!c) {
					continue
				}

				// Remove values reserved for unique enum.
				for (let k = 1; k <= 9; k++) {
					if (ref.possibleValues[k]) {
						removed |= t.possibleValues[k]
						t.possibleValues[k] = false
					}
				}
			}
			if (!removed) {
				continue
			}
			return true
		}
		return false
	}

	pointing = (row, col) => {
		this.rowPointing(row)
		this.columnPointing(col)
	}

	rowPointing = (row) => {
		// TODO: Complicated remove in block will be introduced?
		// This is something I will do all the time.
		// Complicated in that way that I have to ignore few tiles.
		let possibilities = []
		for (let i = 0; i < 9; i++) {
			let block = Math.floor(i / 3)
			if (i % 3 === 0) {
				possibilities[block] = new Array(10).fill(0)
			}
			let t = this.map[row][i]
			if (t.solved) {
				continue
			}
			let vals = t.possibleValues
			for (let j = 1; j <= 9; j++) {
				if (vals[j]) {
					possibilities[block][j]++
				}
			}
		}

		let res = this.hasHiddenSingle(possibilities)
		if (!res.hasSingle) {
			return
		}

		let rows = []
		let rowMultiply = Math.floor(row / 3) * 3
		let ignoredRow = row % 3
		for (let i = 0; i < 3; i++) {
			if (i !== ignoredRow) {
				rows.push(i + rowMultiply)
			}
		}

		for (let j of rows) {
			for (let i = 0; i < 3; i++) {
				let t = this.map[j][i + res.block]
				t.removeFromPossibilities(res.value)
				t.printPossibilities()
			}
		}
	}

	columnPointing = (col) => {
		// TODO: Complicated remove in block will be introduced?
		// This is something I will do all the time.
		// Complicated in that way that I have to ignore few tiles.
		let possibilities = []
		for (let i = 0; i < 9; i++) {
			let block = Math.floor(i / 3)
			if (i % 3 === 0) {
				possibilities[block] = new Array(10).fill(0)
			}
			let t = this.map[i][col]
			if (t.solved) {
				continue
			}
			let vals = t.possibleValues
			for (let j = 1; j <= 9; j++) {
				if (vals[j]) {
					possibilities[block][j]++
				}
			}
		}

		let res = this.hasHiddenSingle(possibilities)
		if (!res.hasSingle) {
			return
		}

		let cols = []
		let ignoredCol = col % 3
		let colMult = Math.floor(col / 3) * 3
		for (let i = 0; i < 3; i++) {
			if (i !== ignoredCol) {
				cols.push(i + colMult)
			}
		}

		for (let i = 0; i < 3; i++) {
			for (let c of cols) {
				let t = this.map[i + res.block][c]
				t.removeFromPossibilities(res.value)
				t.printPossibilities()
			}
		}
	}

	/**
	 * @return {
	 * 	hasSingle: boolean,
	 * 	value: int,
	 * 	block: int
	 * }
	 */
	hasHiddenSingle = (possibilities) => {
		let value = 0
		let block = 0
		for (let i = 1; i <= 9; i++) {
			let res = 0
			for (let j = 0; j < 3; j++) {
				if (possibilities[j][i] > 0) {
					res++
					block = j * 3
				}
			}
			if (res === 1) {
				value = i
				break
			}
		}
		return {
			hasSingle: value > 0,
			value: value,
			block: block,
		}
	}

	// set is a wrapper around value setting and removing.
	// Tile is set, value removed the same way clever sudoku GUI
	// does.
	// It also can call SUDOKULIGA.zaznamPridej, that is based on
	// the value of isInDevelopment property.
	set = (tile, value, col, row) => {
		tile.set(value)
		if (!this.isInDevelopment) {
			SUDOKULIGA.zaznamPridej(0, 'A' + tile.index, tile.value)
		}

		this.remove(col, row, value)
		return true
	}
}

let run = (tm, hardCoef) => {
	tm.setup()
	let times = []
	for (let i = 0; i < tm.toBeSolved; i++) {
		let time = 0
		if (!tm.isInDevelopment) {
			time =
				Math.random() * 1300 * hardCoef +
				Math.random() * 1400 * hardCoef +
				Math.random() * 1500 * hardCoef +
				Math.random() * 1600 * hardCoef +
				Math.random() * 1700 * hardCoef
		}
		times.push(time)
	}

	async function repeater(tm, times) {
		for (let t of times) {
			let wait = (ms) => {
				return new Promise((resolve, _) => {
					setTimeout(resolve, ms)
				})
			}
			await wait(t)
			tm.solve()
		}

		if (!tm.isInDevelopment) {
			time =
				Math.random() * 1300 +
				Math.random() * 1400 +
				Math.random() * 1500 +
				Math.random() * 1600 +
				Math.random() * 1700

			let wait = (ms) => {
				return new Promise((resolve, _) => {
					setTimeout(resolve, ms)
				})
			}
			await wait(time)
			SUDOKULIGA.ligaOdeslat()
		}
	}
	repeater(tm, times)
}

let sl = (inDevelopment, isDiagonal = false, isCenter = false, hardCoef = 1) => {
	let tm = new TileMap(inDevelopment, isDiagonal, isCenter)
	run(tm, hardCoef)
}

let auto = () => {
	let tm = new TileMap(false, false, false)

	let hardCoef = 2.3

	let e = document.getElementById('VA41')
	let b = e.style.background
	switch (b) {
		case 'silver':
			console.log('Center dot')
			hardCoef = 2.9
			tm.isDiagonal = true
			tm.isCenter = true
			break

		case 'rgb(255, 170, 204)':
			console.log('Diagonal')
			hardCoef = 2.7
			tm.isDiagonal = true
			break

		case 'white':
		default:
			if (tm.toBeSolved === 64) {
				console.log('17')
				hardCoef = 3.1
			} else {
				console.log('Classic')
			}
			break

	}
	run(tm, hardCoef)
}
