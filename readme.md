# SUDOKU liga solver

Simple javascript script which can be put to browser
and then run to solve sudoku:

- classic
- 17
- diagonal

on the page sudokuliga.cz.

## Usage

I am using Google Chrome, I hope Firefox is not that special in this case.

1. Add SudokuSolver.js into the Sources -> Snippets
2. Go to the page which sudoku you want to solve
3. Load the script, either right click on it and go "Run", or press CTR-Enter
4. Click "ZOBRAZIT ZADÁNÍ"
5. Write down sl(false, _isDiagonal_, _isCenter_ _hardCoef_), where _isDiagonal_ is a sign if the script should remove duplicates from these two lines, _isCenter_ does something similar (every center in the block creates another block) and _hardCoef_ helps with managing the result time. Classic is OK with 1 (omiting this argument), 17 and diagonal atlast 1.4.
6. After Solver does its job, you can click "ODESLAT ŘEŠENÍ".

If it does not succeed, it writes down "Too hard to be solved". You can finish it by yourself. Note the fact that the tips are not known to SUDOKU liga, so use it carefully, delete it then write down your own.

Phrases mentioning claiming solves debugging purposes. It somehow shows how difficult the sudoku is.

## TODO
- Remember last filled value, it might look nicer than running all over again.

- Overkill with unoptimised pointing?

- ~~You have to solve your issue with hidden pairs triples and anything more.~~

- ~~Center dot sudoku.~~

- ~~Diagonal sudoku.~~

- Making sure it is not faster than the fastest guy.

- Adding an option to be slower than the certain guy, do not win over your friend with this cheating tool.
