<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<title>SUDOKU liga solver &ndash; tester</title>

	<link href="../default.css" rel="stylesheet">
	<link href="../hra.css" rel="stylesheet">
</head>

<body>
	<div id="tabulka">
		<ul id="hranahra" test="vitek"><?php
	require_once 'field.php';

	$r = 1;
	$count = 1;
	foreach ($FIELD as $row) {
		$ext = 'rd';
		if ($r === 1) {
			$ext = 'st';
		} elseif ($r === 2) {
			$ext = 'nd';
		}
		echo "
			<!-- $r$ext row -->";
		foreach ($row as $k => $cell) {
			$m = $cell === 0 ? 1 : 0;
			$c = '';
			if ($k === 2 || $k === 5) {
				$c .= 'P';
			}
			if ($k === 3 || $k === 6) {
				$c .= 'L';
			}
			if ($r === 3 || $r === 6) {
				if ($c !== '') {
					$c .= ' ';
				}
				$c .= 'D';
			}

			if ($r === 4 || $r === 7) {
				if ($c !== '') {
					$c .= ' ';
				}
				$c .= 'H';
			}

			if ($c !== '') {
				$c = " class=\"$c\"";
			}

			$s = '';
			if ($DIAGONAL && ($k + 1 === $r || $k + 1 + $r === 10)) {
				$s = 'style="background: rgb(255, 170, 204);"';
			}

			echo "
			<li id=\"VA$count\"$c muze=\"$m\"$s>
				<input id=\"Ai$count\" maxlength=\"9\" autocomplete=\"off\" class=\"vpisek\">";

			$s = "<input id=\"A$count\"";
			if ($cell !== 0) {
				$s .= " value=\"$cell\"";
			}
			$s .= " class=\"cisloPrvek\" maxlength=\"1\" autocomplete=\"off\"";

			if ($cell !== 0) {
				$s .= " readonly";
			} else {
				$s .= ' style="color: blue; opacity: 1;"';
			}
			$s .= '>';
			echo "
				$s";
			echo "
			</li>";
			$count++;
		}
		$r++;
		echo "\n";
	}
?>
		</ul>
	</div>
</body>

</html>
